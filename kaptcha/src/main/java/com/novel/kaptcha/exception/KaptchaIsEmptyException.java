package com.novel.kaptcha.exception;

/**
 * 验证码为空异常
 *
 * @author novel
 * @date 2019/9/28
 */
public class KaptchaIsEmptyException extends KaptchaException {
    public KaptchaIsEmptyException() {
        super("Kaptcha is  Empty");
    }

    public KaptchaIsEmptyException(String message) {
        super(message);
    }
}
