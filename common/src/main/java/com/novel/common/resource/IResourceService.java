package com.novel.common.resource;

import java.io.IOException;
import java.io.InputStream;

/**
 * 资源服务接口
 *
 * @author novel
 * @date 2019/6/4
 */
public interface IResourceService {
    /**
     * 获取文件的加密地址
     * @param sourceUrl 图片源地址
     * @return 加密后的地址
     * @throws IOException 异常
     */
    String getFileUrl(String sourceUrl) throws IOException;

    /**
     * 上传文件到远程文件服务器
     *
     * @param sourceUrl 源文件地址
     * @param destPath  目标地址
     * @return 文件的访问地址
     * @throws IOException IOException
     */
    boolean upLoadFile(String sourceUrl, String destPath) throws IOException;


    /**
     * 上传文件到远程文件服务器
     * @param source   源文件输入流
     * @param destPath 目标地址
     * @return 文件的访问地址
     * @throws IOException IOException
     */
    boolean upLoadFile(InputStream source, String destPath) throws IOException;


    /**
     * 文件下载
     *
     * @param filePath  源文件地址
     * @param localPath 本地文件地址
     * @throws IOException IOException
     */
    void download(String filePath, String localPath) throws IOException;

    /**
     * 删除文件
     *
     * @param filePath 源文件地址
     * @throws IOException IOException
     */
    boolean delete(String filePath) throws IOException;

    /**
     * 读取文件内容
     *
     * @param filePath 源文件地址
     * @return 文件的字节数组
     * @throws IOException IOException
     */
    byte[] readBytes(String filePath) throws IOException;
}
